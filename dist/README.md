# Webpage Template - README

This is a static website that displays artwork and links to usable tools for makers. The repository can be used as a template for those who similarly want to display their content online.

The target audience for the template is begining web developers, artists, students and educators.

## Usage

click the "Use this template" button to get started.

I use certain applications when I am working on this project that you may find useful. They are not depependencies of this project but some like "Git" are highly recomended.
- [VSCode](https://code.visualstudio.com/)
- [Git](https://git-scm.com/)
- [GitHub Desktop](https://desktop.github.com/)
- [Node.js](https://nodejs.org/en/)
- [npm](https://www.npmjs.com/)
- [Pug](https://pugjs.org/)
- [Sass](https://sass-lang.com/)
- [Typescript](https://www.typescriptlang.org/)
- [Jest](https://jestjs.io/)
- [Prettier](https://prettier.io/)
- [#region folding](https://marketplace.visualstudio.com/items?itemName=maptz.regionfolder)

## Usage

Examples of how to use your project.

## Contributing

[Contribution](Contributing.md)

## License

[License](License.md)

## Contact

Your contact information.

Hi, I am a sculptor and adjunct professor that uses this repository as the codebase for my personal website, (www.kylehossli.com). It also serves as a place for me to test out different techniques related to web development. Clone the site to start building with it as a template. Once you have your copy you can toss anything you don't need, add your own content, and change the style of the site as you please.

I hold a copyright for the artworks and the images of those artworks, but aside from using them as representations of your own artwork you can do whatever you want with them. To remove the images identify the name of the html page you would like to scrub, then find the folder in the "media_assets" directory with the same name as the html file and delete the image files inside it. To remove all the photos from the website at once delete the contents of every folder in the "media_assets" directory.