{
  "extends": [
    "stylelint-config-recommended",
    "stylelint-config-standard",
    "stylelint-config-idiomatic-order",
    "stylelint-config-html",
    "stylelint-config-recommended-vue"
  ],

  "plugins": [
    "stylelint-declaration-block-no-ignored-properties",
    "stylelint-plugin-defensive-css"
  ],

  "rules": {
    "annotation-no-unknown": true,
    "at-rule-no-vendor-prefix": true,
    "at-rule-name-case": "lower",
    "at-rule-name-space-after": "always-single-line",
    "at-rule-semicolon-newline-after": "always",
    "at-rule-semicolon-space-before": "never",
    "at-rule-no-unknown": [
      true,
      {
        "ignoreAtRules": [
          "at-root",
          "debug",
          "each",
          "else",
          "error",
          "extend",
          "for",
          "function",
          "import",
          "if",
          "include",
          "media",
          "mixin",
          "return",
          "use",
          "warn",
          "while"
        ]
      }
    ],
    "at-rule-empty-line-before": [
      "always",
      {
        "ignore": ["after-comment", "first-nested", "inside-block"],
        "except": ["blockless-after-same-name-blockless"]
      }
    ],
    "block-no-empty": true,
    "color-function-notation": "modern",
    "color-hex-case": "lower",
    "color-hex-length": "long",
    "color-no-invalid-hex": true,
    "color-named": [
      "never",
      {
        "ignore": ["inside-function"]
      }
    ],
    "comment-empty-line-before": "always",
    "comment-no-empty": true,
    "comment-whitespace-inside": "always",
    "comment-word-disallowed-list": ["/^TODO:/"],
    "custom-media-pattern": null,
    "custom-property-empty-line-before": "never",
    "custom-property-no-missing-var-function": true,
    "custom-property-pattern": null,
    "declaration-bang-space-after": "never",
    "declaration-bang-space-before": "always",
    "declaration-colon-newline-after": "always-multi-line",
    "declaration-colon-space-after": "always-single-line",
    "declaration-colon-space-before": "never",
    "declaration-block-semicolon-newline-after": "always",
    "declaration-block-semicolon-space-before": "never",
    "declaration-block-trailing-semicolon": "always",
    "declaration-block-no-duplicate-properties": true,
    "declaration-property-unit-disallowed-list": [
      {
        "font-size": ["px"]
      }
    ],
    "declaration-block-no-duplicate-custom-properties": true,
    "declaration-block-no-redundant-longhand-properties": true,
    "declaration-block-no-shorthand-property-overrides": true,
    "declaration-empty-line-before": "never",
    "declaration-property-value-no-unknown": true,
    "font-family-name-quotes": "always-where-recommended",
    "font-family-no-duplicate-names": true,
    "font-family-no-missing-generic-family-keyword": true,
    "font-weight-notation": "numeric",
    "function-calc-no-unspaced-operator": true,
    "function-comma-newline-after": "never-multi-line",
    "function-comma-newline-before": "never-multi-line",
    "function-comma-space-after": "always-single-line",
    "function-comma-space-before": "never-single-line",
    "function-disallowed-list": null,
    "function-linear-gradient-no-nonstandard-direction": true,
    "function-max-empty-lines": 0,
    "function-name-case": "lower",
    "function-no-unknown": true,
    "function-url-no-scheme-relative": null,
    "function-url-scheme-allowed-list": ["data", "https"],
    "function-url-scheme-disallowed-list": ["ftp", "/^http/"],
    "function-parentheses-newline-inside": "never-multi-line",
    "function-parentheses-space-inside": "never",
    "function-url-quotes": "always",
    "function-whitespace-after": "always",
    "hue-degree-notation": "angle",
    "keyframe-block-no-duplicate-selectors": true,
    "keyframe-declaration-no-important": true,
    "keyframe-selector-notation": "percentage",
    "keyframes-name-pattern": null,
    "length-zero-no-unit": true,
    "lightness-notation": "percentage",
    "max-empty-lines": 2,
    "max-nesting-depth": 2,
    "media-feature-name-no-unknown": true,
    "media-feature-name-no-vendor-prefix": true,
    "media-feature-name-value-no-unknown": true,
    "media-feature-range-notation": "prefix",
    "media-query-no-invalid": true,
    "named-grid-areas-no-invalid": true,
    "no-extra-semicolons": true,
    "no-descending-specificity": true,
    "no-duplicate-selectors": true,
    "no-empty-source": true,
    "no-invalid-double-slash-comments": true,
    "no-invalid-position-at-import-rule": true,
    "no-irregular-whitespace": true,
    "no-unknown-animations": true,
    "no-unknown-custom-properties": null,
    "number-max-precision": 5,
    "number-leading-zero": "always",
    "number-no-trailing-zeros": true,
    "plugin/declaration-block-no-ignored-properties": true,

    "plugin/use-defensive-css": {
      "accidental-hover": true,
      "background-repeat": true,
      "custom-property-fallbacks": true,
      "flex-wrapping": true,
      "scroll-chaining": true,
      "scrollbar-gutter": true,
      "vendor-prefix-grouping": true
    },
    "property-case": "lower",
    "property-no-unknown": true,
    "property-no-vendor-prefix": true,
    "rule-empty-line-before": "never",
    "selector-attribute-brackets-space-inside": "never",
    "selector-attribute-operator-space-after": "never",
    "selector-attribute-operator-space-before": "never",
    "selector-attribute-quotes": "always",
    "selector-combinator-space-before": "always",
    "selector-pseudo-class-case": "lower",
    "selector-pseudo-class-parentheses-space-inside": "never",
    "selector-pseudo-element-case": "lower",
    "selector-pseudo-element-colon-notation": "double",
    "selector-type-case": "lower",
    "selector-combinator-space-after": "always",
    "selector-list-comma-newline-after": "always",
    "selector-list-comma-newline-before": "never-multi-line",
    "selector-anb-no-unmatchable": true,
    "selector-id-pattern": null,
    "selector-max-id": 0,
    "selector-max-specificity": "1,1,1",
    "selector-max-universal": 3,
    "selector-nested-pattern": null,
    "selector-no-vendor-prefix": true,
    "selector-not-notation": "complex",
    "selector-pseudo-class-no-unknown": true,
    "selector-pseudo-element-no-unknown": true,
    "selector-type-no-unknown": [
      true,
      {
        "ignore": ["custom-elements"]
      }
    ],
    "shorthand-property-no-redundant-values": true,
    "string-no-newline": true,
    "string-quotes": "double",
    "time-min-milliseconds": 100,
    "unit-no-unknown": true,
    "unit-case": "lower",
    "value-keyword-case": "lower",
    "value-list-comma-newline-before": "never-multi-line",
    "value-list-comma-space-after": "always-single-line",
    "value-list-comma-space-before": "never-single-line",
    "value-list-comma-newline-after": "always-multi-line",
    "value-list-max-empty-lines": 0,
    "value-no-vendor-prefix": [
      true,
      {
        "ignoreValues": ["box", "inline-box"]
      }
    ]
  },
  "overrides": [
    {
      "files": ["*.html", "**/*.html"],
      "customSyntax": "postcss-html"
    }
  ]
}
